<?php
/**
 * Created by Jannis Cyron 11.02.18
 * User: jacyro
 * Date: 11.02.2018
 * Time: 15:52
 * Project: Restart RPi if octoprint status is not printing:true
 *
 * Needs:
 *  * PHP REST Client
 *  * https://github.com/tcdent/php-restclient
 *  * (c) 2013-2017 Travis Dent <tcdent@gmail.com>
 *
 * Setup:
 *  * sudo apt-get install curl libcurl3 libcurl3-dev php5-curl php5-cli
 *  * Copy files to ~/reboot
 *  * crontab -e
 *  * Add: 0 *12 * * * php ~/reboot/api_based_reboot.php
 *
 */

require('restclient.php');

const OCTOPRINT_BASE_URL = 'http://localhost/';
const OCTOPRINT_API_KEY = '<YOUR-API-KEY>';
const PI_ROOT_PW = '<YOUR-ROOT-PW>';

$api = new RestClient([
    'base_url' => OCTOPRINT_BASE_URL,
    'headers' => ['X-Api-Key' => OCTOPRINT_API_KEY],
]);
$result = $api->get("api/printer");

if($result->info->http_code == 200) {
    $objResponse = json_decode($result->response);
    echo "Printing?";
    if ($objResponse->state->flags->printing == true) {
        echo "True - Reboot forbidden.";
    } else {
        echo "False - Reboot allowed.";

        $command = "echo " . PI_ROOT_PW . " | su -c 'reboot now'";
        $output = array();
        try{
            echo shell_exec($command);
            exec($command, $output);
            system($command, $output);
        }Catch(Exception $e){
            print "Unable to shutdown system...";
        }
    }
}
